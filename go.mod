module sms-service

go 1.14

require (
	cloud.google.com/go v0.90.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gocraft/web v0.0.0-20190207150652-9707327fb69b
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/tealeg/xlsx v1.0.5
	github.com/xuri/excelize/v2 v2.4.1 // indirect
	golang.org/x/text v0.3.6 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
