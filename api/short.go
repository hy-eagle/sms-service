package api

import (
	"fmt"
	"github.com/gocraft/web"
	"log"
	"net/http"
	"strings"
)

type Short struct {
	*RootContext
}

func (self *Short) ShortUrlJump(w web.ResponseWriter, r *web.Request) {
	decodeStr := r.PathParams["decode"]
	at := 100
	at = strings.Index(decodeStr, "-") //at == 0 orderNo
	params := "?order_no="
	var sr SmsReport

	if at == 100 || at == -1 {
		log.Println("wrong at:", at, " decodeStr:", decodeStr)
		return
	}

	if at == 0 {
		rawOrderNo := strings.TrimLeft(decodeStr, "-")
		orderNo := Decode(rawOrderNo)
		sr.OrderNo = fmt.Sprintf("%d",orderNo)
		params += fmt.Sprintf("%d",orderNo) + "&phone="
	} else {
		rawPhone := strings.TrimRight(decodeStr, "-")
		phone := Decode(rawPhone)
		sr.Phone = fmt.Sprintf("%d", phone)
		if len(sr.Phone) > 13 {
			log.Println("decode phone err, decodeStr:", decodeStr, " sr.Phone:", sr.Phone)
		}
		params += "&phone=" + fmt.Sprintf("%d", phone)
	}

	jumpUrl := "http://cmp3.hicall.ai/sms/" + params
	sr.Type = "jump"
	if err := GormDbSms.Create(&sr).Error; err != nil {
		log.Println("ShortUrlJump save err:", err, " sr:", sr)
	}

 	http.Redirect(w, r.Request, jumpUrl, http.StatusFound)
}
