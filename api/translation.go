package api

import (
	"cloud.google.com/go/translate"
	"context"
	"errors"
	"fmt"
	"github.com/gocraft/web"
	"github.com/xuri/excelize/v2"
	"golang.org/x/text/language"
	"io"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

type Api struct {
	*RootContext
}

func (self *Api) DownloadFile(w web.ResponseWriter, r *web.Request) {
	fileName := ""
	if t, ok := r.Request.URL.Query()["file_name"]; ok {
		fileName = t[0]
	}

	if fileName != "" {
		file, err := os.Open(fileName)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer file.Close()
		content, err := ioutil.ReadAll(file)
		fileNames := url.QueryEscape(fileName) // 防止中文乱码
		w.Header().Add("Content-Type", "application/octet-stream")
		w.Header().Add("Content-Disposition", "attachment; filename=\""+fileNames+"\"")

		if err != nil {
			fmt.Println("Read File Err:", err.Error())
		} else {
			w.Write(content)
		}

		os.Remove(fileName)
	}
}

func (self *Apn) Upload(w web.ResponseWriter, r *web.Request) {
	file, header, err := r.FormFile("excel_file")
	if err != nil {
		log.Println("FormFile err:", err)
		return
	}

	// 将文件拷贝到指定路径下，或者其他文件操作
	dst, err := os.Create(header.Filename)
	if err != nil {
		log.Println("Create err:", err)
		return
	}
	_, err = io.Copy(dst, file)
	if err != nil {
		log.Println("Copy err:", err)
		return
	}

	err = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "/home/work/sms_service/HY-c76b188d6699.json")
	if err != nil {
		fmt.Println("ERROR:GOOGLE_APPLICATION_CREDENTIALS SET----", err.Error())
	}
	err = UploadExcel(header.Filename)
	if err != nil {
		self.okPacket.Code = -1
		self.okPacket.Message = err.Error()
		return
	}
	os.Remove(header.Filename)
	downloadFileName := "结果" + header.Filename
	self.okPacket.Code = 1
	self.okPacket.Data = map[string]interface{}{
		"download_url" : "http://jp.hicall.ai/api/downloadFile?file_name=" + downloadFileName,
	}
}

func RemoveRepByMap(slc []string) []string {
	result := []string{}
	tempMap := map[string]byte{}  // 存放不重复主键
	for _, e := range slc{
		l := len(tempMap)
		tempMap[e] = 0
		if len(tempMap) != l{  // 加入map后，map长度变化，则元素不重复
			result = append(result, e)
		}
	}
	return result
}

func translateText(targetLanguage, text string, sourceLan language.Tag) (string, error) {
	//return "d", nil
	// text := "The Go Gopher is cute"
	ctx := context.Background()

	lang, err := language.Parse(targetLanguage)
	if err != nil {
		return "", fmt.Errorf("language.Parse: %v", err)
	}

	client, err := translate.NewClient(ctx)
	if err != nil {
		return "", err
	}
	defer client.Close()

	resp, err := client.Translate(ctx, []string{text}, lang, &translate.Options{
		Source: sourceLan,
	})
	if err != nil {
		return "", fmt.Errorf("Translate: %v", err)
	}
	if len(resp) == 0 {
		return "", fmt.Errorf("Translate returned empty response to text: %s", text)
	}
	return resp[0].Text, nil
}

func UploadExcel(fileName string) error {
	f, err := excelize.OpenFile(fileName)
	if err != nil {
		fmt.Println("OpenFile err:",err)
		return errors.New("打开文件出错")
	}

	rows, err := f.GetRows("订单")
	if err != nil {
		fmt.Println("sheet 不存在")
		return errors.New("订单不存在")
	}

	newRows, err := DealData(rows)
	if err != nil {
		return err
	}
	SaveFile("结果" + fileName, newRows)

	return nil
}

func SaveFile(fileName string, rows [][]string){
	keyMap := map[int]string{
		0:"A",
		1:"B",
		2:"C",
		3:"D",
		4:"E",
		5:"F",
		6:"G",
		7:"H",
	}

	f := excelize.NewFile()
	// Create a new sheet.
	index := f.NewSheet("Sheet1")
	// Set value of a cell.
	f.SetCellValue("Sheet1", "A1", "平台")
	f.SetCellValue("Sheet1", "B1", "姓名")
	f.SetCellValue("Sheet1", "C1", "电话")
	f.SetCellValue("Sheet1", "D1", "日期")
	f.SetCellValue("Sheet1", "E1", "金额")
	f.SetCellValue("Sheet1", "F1", "泰文")
	f.SetCellValue("Sheet1", "G1", "订单号")
	f.SetCellValue("Sheet1", "H1", "店铺")
	for k, row := range rows {
		for kk, r := range row {
			f.SetCellValue("Sheet1", keyMap[kk] + fmt.Sprintf("%d", k + 2), r)
		}
	}

	// Set active sheet of the workbook.
	f.SetActiveSheet(index)
	// Save spreadsheet by the given path.
	if err := f.SaveAs(fileName); err != nil {
		fmt.Println("save file err:", err)
	}
}

func DealData(rows [][]string) ([][]string, error) {
	var chineseMap = make(map[string]map[string]bool)
	var countMap = make(map[string]int)
	var priceMap = make(map[string][]string)
	var nameMap = make(map[string]string)
	var phoneMap = make(map[string]string)
	var addressMap = make(map[string]string)
	var platformMap = make(map[string]string)
	var storeMap = make(map[string]string)
	var orderNoMap = make(map[string][]string)

	for _, row := range rows {
		if len(row) < 19 {
			return nil, errors.New("请检查是否缺失某列数据")
		}
		chineseMapKey := row[12] + row[13] + row[18] // 电话-平台
		nameMap[chineseMapKey] = row[11]
		phoneMap[chineseMapKey] = row[12] + row[13]
		addressMap[chineseMapKey] = row[14]
		priceMap[chineseMapKey] = append(priceMap[chineseMapKey], row[3])
		platformMap[chineseMapKey] = row[18]
		storeMap[chineseMapKey] = row[17]
		orderNoMap[chineseMapKey] = append(orderNoMap[chineseMapKey],row[0])

		if v, ok := chineseMap[chineseMapKey]; ok {
			if _, ok := v[row[8]]; !ok {
				v[row[8]] = true
				chineseMap[chineseMapKey] = v
			}
		} else {
			tmp := make(map[string]bool)
			tmp[row[8]] = true
			chineseMap[chineseMapKey] = tmp
		}

		countKey := chineseMapKey + "-" + row[8]
		goodsNum,_ := strconv.Atoi(row[6])
		if _, ok := countMap[countKey]; ok {
			countMap[countKey] += goodsNum
		} else {
			countMap[countKey] += goodsNum
		}
	}

	chineseResult := make([][]string,0)
	for phonePlat,v := range chineseMap {
		tmp := ""
		count := 0
		i := 0

		onesame := false
		twosame := false
		twodifferent := false
		conjunction := ""
		if len(v) >= 2 {
			twodifferent = true
			conjunction = " และ "
		}

		for name, _ := range v {
			countKey := phonePlat + "-" + name
			singleCount := countMap[countKey]
			count += singleCount

			if len(v) == 1 {
				if singleCount >= 2 {
					twosame = true
				} else {
					onesame = true
				}
			}


			if i >= 2 {
				break
			} else {
				tmp += name + conjunction
			}
			i++
		}
		tmp = strings.TrimRight(tmp, conjunction)

		thai,err := translateText("tha", tmp, language.Chinese)
		if err != nil {
			fmt.Println("translateText thai err:", err, " tmp: ", tmp)
			return nil,err
		}

		th := ""
		if onesame {
			th = fmt.Sprintf("%d", count) + "ชิ้น คือ" + thai
		} else if twosame {
			th = thai + fmt.Sprintf("%d", count) + "ชชิ้น"
		} else if twodifferent {
			th = thai + " เป็นต้น"
		}

		phoneNum := strings.Replace(phoneMap[phonePlat],"-","",-1)
		phoneNum = strings.Replace(phoneNum," ","",-1)
		phoneNum = strings.Replace(phoneNum,"+","",-1)
		if strings.HasPrefix(phoneNum, "660")  {
			phoneNum = "'+'66" + phoneNum[3:]
		} else if strings.HasPrefix(phoneNum, "0") {
			phoneNum = "'+'66" + phoneNum[1:]
		} else {
			phoneNum = "'+'" + phoneNum
		}
		//fmt.Println("has prefix:", phoneNum)
		orderNos := RemoveRepByMap(orderNoMap[phonePlat])
		prices := RemoveRepByMap(priceMap[phonePlat])
		totalPrice := 0.0
		for _,p := range prices {
			price, _ := strconv.ParseFloat(p,64)
			totalPrice += price
		}
		chineseResult = append(chineseResult,  []string{platformMap[phonePlat], nameMap[phonePlat], phoneNum, time.Now().Format("2006-01-02"), fmt.Sprintf("%f",totalPrice), th, "orderNo:" + strings.Join(orderNos, ","), storeMap[phonePlat] })
	}

	return chineseResult, nil
}
