package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gocraft/web"
	"github.com/tealeg/xlsx"
	"gorm.io/gorm"
	"io/ioutil"
	"log"
	"math"
	"mime/multipart"
	"net/http"
	"sms-service/service"
	"strconv"
	"strings"
	"time"
)

type OkPacket struct {
	Code    int32       `json:"error_code"`
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"error_msg,omitempty"`
}

type RootContext struct {
	okPacket *OkPacket
	//DO NOT set it message manually
	err interface{}
}


type Apn struct {
	*RootContext
}

type DetailInfo struct {
	Detail string
	Attr string
	Code string
	//Amount int
}

type GoodsInfo struct {
	Id          int
	OrderNo     string
	TotalFee    int
	ExpressFee  int
	GoodsAmount int
	Receiver    string
	Phone       string
	Store       string
	Platform    string
	Date        string
	Code        string
	Attr        string
	GoodsDetail string
}

type ExcelGoodsInfo struct {
	OrderNo     string
	TotalFee    int
	ExpressFee  int
	GoodsAmount int
	Receiver    string
	Phone       string
	DetailInfo  DetailInfo
	Store       string
	Platform    string
	Date        string
	Code        string
	Attr        string
	GoodsDetail string
}

var DbSms *sql.DB
var GormDbSms *gorm.DB

func (self *RootContext) Acao(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "content-type")
	next(w, r)
}

func (self *Apn) PrePackMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	//TODO: Use this function to send response automatically
	defer func() {
		if self.err == nil  {
			data,_ := json.Marshal(self.okPacket)
			w.Write(data)
		}
	}()

	self.okPacket = &OkPacket{}

	next(w, r)
}


type SendRecord struct {
	Id int `json:"id"`
	SendStatus string `json:"send_status"`
	OrderNo string `json:"order_no"`
	Phone string `json:"phone"`
	CreatedAt time.Time `json:"created_at"`
}

func (self *Apn) SendMsgForTest(w web.ResponseWriter, r *web.Request) {
	date := ""
	orderNo := ""
	phone := ""
	if t, ok := r.Request.URL.Query()["date"]; ok {
		date = t[0]
	}

	if t, ok := r.Request.URL.Query()["order_no"]; ok {
		orderNo = t[0]
	}

	if t, ok := r.Request.URL.Query()["phone"]; ok {
		phone = t[0]
	}

	if date == "" {
		date = time.Now().Format("2006-01-02")
	}

	whereCond := ""
	data := make([]GoodsInfo,0)
	rawShortId := int64(0)
	paramType := 0
	if orderNo != "" && phone == "" {
		rawShortId,_ = strconv.ParseInt(orderNo, 10, 64)
		paramType = 1
		whereCond += fmt.Sprintf("order_no = '%s' and date = '%s'", orderNo, date)
	} else if orderNo == "" && phone != "" {
		rawShortId,_ = strconv.ParseInt(phone, 10, 64)
		paramType = 2
		whereCond += fmt.Sprintf("phone = '%s' and date = '%s'", phone, date)
	}

	if err := GormDbSms.Where(whereCond).Find(&data).Error; err != nil && err != gorm.ErrRecordNotFound {
		log.Println("SendMsg get info err:", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "内部错误"
		return
	}

	if len(data) == 0 {
		log.Println("no data, whereCond is:", whereCond)
		return
	}

	shortId := Encode(rawShortId)
	if paramType == 1 {
		shortId = "-" + shortId
	} else if paramType == 2 {
		shortId = shortId + "-" // phone
	}

	shortUrl := "http://jp.hicall.ai/" + shortId
	msgContent := "「ร้าน"+ data[0].Store +"」นาย/นาง "+data[0].Receiver+" ที่เคารพ，เนื่องจากว่ายอดสั่งซื้อของคุณค่อนข้างสูง，โปรดกดลิงค์เพื่อยืนยันข้อมูลการสั่งซื้อของคุณ "+ shortUrl +"，หลังจากที่ยืนยันการสั่งซื้อแล้ว เราจะส่งพัสดุของคุณโดยเร็วที่สุด"


	self.okPacket.Data = map[string]interface{}{
		"payload":msgContent,
		"where":whereCond,
	}

	return
}

//只允许使用phone + date来发送
func (self *Apn) SendMsg(w web.ResponseWriter, r *web.Request) {
	date := ""
	orderNo := ""
	phone := ""
	if t, ok := r.Request.URL.Query()["date"]; ok {
		date = t[0]
	}

	if t, ok := r.Request.URL.Query()["order_no"]; ok {
		orderNo = t[0]
	}

	if t, ok := r.Request.URL.Query()["phone"]; ok {
		phone = t[0]
	}

	if date == "" {
		date = time.Now().Format("2006-01-02")
	}

	whereCond := ""
	data := make([]GoodsInfo,0)
	rawShortId := int64(0)
	paramType := 0
	if orderNo != "" && phone == "" {
		rawShortId,_ = strconv.ParseInt(orderNo, 10, 64)
		paramType = 1
		whereCond += fmt.Sprintf("order_no = '%s' and date = '%s'", orderNo, date)
	} else if orderNo == "" && phone != "" {
		rawShortId,_ = strconv.ParseInt(phone, 10, 64)
		paramType = 2
		whereCond += fmt.Sprintf("phone = '%s' and date = '%s'", phone, date)
	}

	if err := GormDbSms.Where(whereCond).Find(&data).Error; err != nil && err != gorm.ErrRecordNotFound {
		log.Println("SendMsg get info err:", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "内部错误"
		return
	}

	if len(data) == 0 {
		log.Println("no data, whereCond is:", whereCond)
		return
	}

	result := SmsResult{}

	shortId := Encode(rawShortId)
	if paramType == 1 {
		shortId = "-" + shortId
	} else if paramType == 2 {
		shortId = shortId + "-"
	}

	shortUrl := "http://jp.hicall.ai/" + shortId
	msgContent := "「ร้าน"+ data[0].Store +"」นาย/นาง "+data[0].Receiver+" ที่เคารพ，เนื่องจากว่ายอดสั่งซื้อของคุณค่อนข้างสูง，โปรดยืนยันข้อมูลการสั่งซื้อของคุณโดยเร็วที่สุด "+ shortUrl
	//body["uuid"] = fmt.Sprintf("%d",rawShortId)
	//body["isps_key"] = "sms-nx-th"
	//body["callee"] = data[0].Phone
	//body["content"] = msgContent
	//body["max_retry_count"] = "3"
	//header["Content-Type"]="application/json"
	//header["Authorization"]="3f33rvuEEra3C2VphC3Hz9nk5XXVIePr"
	//resp, err := Post("http://172.16.1.7:10125/internal/send", body, nil, header)
	url := "http://127.0.0.1:10125/internal/send"
	method := "POST"

	payload := strings.NewReader(`{
    "uuid":"`+fmt.Sprintf("%d",rawShortId)+`",
    "isps_key":"sms-nx-th",
    "callee":"`+data[0].Phone+`",
    "content":"`+msgContent+`",
    "max_retry_count":2
}`)

	client := &http.Client {
	}
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		log.Println("send msg new request err:",err)
		return
	}
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "3f33rvuEEra3C2VphC3Hz9nk5XXVIePr")

	resp, err := client.Do(req)
	if err != nil {
		log.Println("send msg post err:", err, " resp:", resp)
		return
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		self.okPacket.Code = -1
		self.okPacket.Message = "读取结果失败"
		log.Println("send msg ReadAll err:", err)
		return
	}
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		self.okPacket.Code = -1
		self.okPacket.Message = "解析结果失败"
		log.Println("send msg Unmarshal err:", err, " resp:", resp)
		return
	}

	var sr SendRecord
	sr.OrderNo = data[0].OrderNo
	sr.Phone = phone
	if result.Message != "success" {
		sr.SendStatus = "failed"
		self.okPacket.Code = -1
		self.okPacket.Message = "短信服务失败"
		log.Println("send msg err:", err, " result:", result)
	} else {
		sr.SendStatus = "success"
		self.okPacket.Code = 1
		self.okPacket.Message = "发送成功"
	}

	if err:=GormDbSms.Create(&sr).Error; err != nil {
		log.Println("send msg create err:", err)
	}
}

type SmsResult struct {
	Code int `json:"code"`
	Message string `json:"message"`
}

func PostWithFormData(method, url string, postData map[string]string, header map[string]string) (*http.Response, error) {
	body := new(bytes.Buffer)
	w := multipart.NewWriter(body)
	for k,v :=  range postData{
		w.WriteField(k, v)
	}
	w.Close()
	req, _ := http.NewRequest(method, url, body)
	req.Header.Set("Content-Type", w.FormDataContentType())
	for k,v := range header {
		req.Header.Set(k,v)
	}
	return http.DefaultClient.Do(req)
}


func Post(url string, body interface{}, params map[string]string, headers map[string]string) (*http.Response, error) {
	//add post body
	var bodyJson []byte
	var req *http.Request
	if body != nil {
		var err error
		bodyJson, err = json.Marshal(body)
		if err != nil {
			return nil, errors.New("http post body to json failed")
		}
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(bodyJson))
	if err != nil {
		return nil, errors.New("new request is fail: %v \n")
	}
	req.Header.Set("Content-type", "application/json")

	//add params
	q := req.URL.Query()
	if params != nil {
		for key, val := range params {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}

	//add headers
	if headers != nil {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}

	//http client
	client := &http.Client{Timeout:time.Second * 60}
	return client.Do(req)
}

func (self *Apn) GetGoodsInfo(w web.ResponseWriter, r *web.Request) {
	orderNo := ""
	phone := ""
	if t, ok := r.Request.URL.Query()["order_no"]; ok {
		orderNo = t[0]
	}

	if t, ok := r.Request.URL.Query()["phone"]; ok {
		phone = t[0]
	}

	whereCond := ""
	data := make([]GoodsInfo,0)
	//date := time.Now().Format("2006-01-02")
	if orderNo != "" && phone == "" {
		whereCond += fmt.Sprintf("order_no = '%s'", orderNo)
	} else if orderNo == "" && phone != "" {
		whereCond += fmt.Sprintf("phone = '%s'", phone)
	}

	if err := GormDbSms.Where(whereCond).Find(&data).Error; err != nil && err != gorm.ErrRecordNotFound {
		log.Println("GetGoodsInfo err:", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "内部错误"
		return
	}

	if len(data) == 0 {
		self.okPacket.Code = -1
		self.okPacket.Message = "无数据"
		return
	}

	expressFee := 0.0
	orderFee := 0.0
	totalFee := 0.0
	expressFee = float64(data[0].ExpressFee) / 100
	totalFee = float64(data[0].TotalFee) / 100
	orderFee = totalFee - expressFee

	self.okPacket.Code = 1
	self.okPacket.Message = "获取成功"
	self.okPacket.Data = map[string]interface{}{
		"express_fee":expressFee,
		"order_fee":orderFee,
		"total_fee":totalFee,
		"data": data,
	}
}

const CODE62  = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const CODE_LENTH = 62
var EDOC = map[string]int64{"0":0,"1":1,"2":2,"3":3,"4":4,"5":5,"6":6,"7":7,"8":8,"9":9,"a":10,"b":11,"c":12,"d":13,"e":14,"f":15,"g":16,"h":17,"i":18,"j":19,"k":20,"l":21,"m":22,"n":23,"o":24,"p":25,"q":26,"r":27,"s":28,"t":29,"u":30,"v":31,"w":32,"x":33,"y":34,"z":35,"A":36,"B":37,"C":38,"D":39,"E":40,"F":41,"G":42,"H":43,"I":44,"J":45,"K":46,"L":47,"M":48,"N":49,"O":50,"P":51,"Q":52,"R":53,"S":54,"T":55,"U":56,"V":57,"W":58,"X":59,"Y":60,"Z":61, }

// Encode /**
func Encode(number int64) string {
	if number == 0 {
		return "0"
	}
	result := make([]byte , 0)
	for number > 0 {
		round  := number / CODE_LENTH
		remain := number % CODE_LENTH
		result = append(result,CODE62[remain])
		number  = round
	}
	return string(result)
}

// Decode /**
func Decode(str string) int64 {
	str = strings.TrimSpace(str)
	var result int64 = 0
	for index,char := range []byte(str){
		result +=  EDOC[string(char)] * int64(math.Pow(CODE_LENTH,float64(index)))
	}
	return result
}

type SmsReport struct{
	Id int `json:"id"`
	OrderNo string `json:"order_no"`
	Phone string `json:"phone"`
	Type string `json:"type"`
	Result string `json:"result"`
	Ip string `json:"ip"`
	CreatedAt time.Time `json:"created_at"`
}

func (self *Apn)Report(w web.ResponseWriter, r *web.Request) {
	var sr SmsReport
	if t, ok := r.Request.URL.Query()["order_no"]; ok {
		sr.OrderNo = t[0]
	}
	if t, ok := r.Request.URL.Query()["phone"]; ok {
		if len(t[0]) > 13 {
			req := fmt.Sprintf("%+v", r.Request.URL.Query())
			log.Println("illeage phone: req:", req)
		}
		sr.Phone = t[0]
	}
	if t, ok := r.Request.URL.Query()["type"]; ok {
		if t[0] == "" {
			req := fmt.Sprintf("%+v", r.Request.URL.Query())
			log.Println("type is empty, req:", req)
			self.okPacket.Code = -1
			self.okPacket.Message = "确认失败"
			return
		}
		sr.Type = t[0]
	}
	if t, ok := r.Request.URL.Query()["result"]; ok {
		sr.Result = t[0]
	}

	sr.Ip = service.RealIP(r)

	if err := GormDbSms.Create(&sr).Error; err != nil {
		log.Println("save report err:", err, " sr:", sr)
	}

	self.okPacket.Code = 1
	self.okPacket.Message = "确认成功"
}

func (self *Apn) SaveExcel(w web.ResponseWriter, r *web.Request) {
	filePath := "goods.xlsx"
	if t, ok := r.Request.URL.Query()["file_path"]; ok {
		filePath = t[0]
	}
	data := ExcelParse(filePath)
	mergedData := MergeProduct(data)
	SaveData(mergedData)
}

func ExcelParse(filePath string) map[string][]ExcelGoodsInfo {
	dataMap := make(map[string][]ExcelGoodsInfo)
	xlFile, err := xlsx.OpenFile(filePath)
	if err != nil {
		fmt.Println("Open file err:", err)
	}
	//checkErr(err)//自己定义的函数
	//获取行数
	//length := len(xlFile.Sheets[0].Rows)

	//遍历sheet
	for _, sheet := range xlFile.Sheets {
		//遍历每一行
		for rowIndex, row := range sheet.Rows {
			//跳过第一行表头信息
			if rowIndex == 0 {
				continue
			}

			var tmp ExcelGoodsInfo
			var key string //订单号-手机
			//遍历每一个单元
			var detailsArr = make([]DetailInfo,0)
			for cellIndex, cell := range row.Cells {
				switch cellIndex {
				case 0:
					tmp.OrderNo = cell.Value
					key += cell.Value
				case 2:
					expressFee,_ := strconv.ParseFloat(cell.Value,64)
					tmp.ExpressFee = int(expressFee*100)
				case 3:
					totalFee,_ := strconv.ParseFloat(cell.Value,64)
					tmp.TotalFee = int(totalFee*100)
				case 6:
					tmp.GoodsAmount,_ = strconv.Atoi(cell.Value)
				case 8:
					tmp.Receiver = cell.Value
				case 9:
					if cell.Value != "" {
						if strings.HasPrefix(cell.Value, "660") {
							tmp.Phone = "66" + cell.Value[3:]
							key += "-" + cell.Value
						} else {
							key += "-" + cell.Value
							tmp.Phone = cell.Value
						}
					}
				case 10:
					if cell.Value != "" {
						if strings.HasPrefix(cell.Value, "660") {
							tmp.Phone = "66" + cell.Value[3:]
							key += "-" + cell.Value
						} else if strings.HasPrefix(cell.Value, "0") {
							tmp.Phone = "66" + cell.Value[1:]
							key += "-" + cell.Value
						} else {
							key += "-" + cell.Value
							tmp.Phone = cell.Value
						}
					}
				case 13:
					var goodDetail DetailInfo

					details := strings.Split(cell.Value, "\n")
					if len(details) != 5 {
						for {
							if len(details) >= 5 {
								var tmpDetail DetailInfo
								detailN := details[:4]
								tmpDetail.Detail = detailN[0]
								tmpAttr := strings.Replace(detailN[1], "(产品属性:{\"属性\":\"", "", -1)
								tmpAttr = strings.Replace(tmpAttr, "(产品属性:{", "", -1)
								tmpDetail.Attr = strings.Replace(tmpAttr, "})", "", -1)
								tmpCode := strings.Replace(detailN[2], "(商家编码:", "", -1)
								tmpDetail.Code = strings.Replace(tmpCode, ")", "", -1)
								details = details[4:]
								detailsArr = append(detailsArr, tmpDetail)
							} else {
								break
							}
						}
					} else {
						goodDetail.Detail = details[0]
						attr := strings.Replace(details[1], "(产品属性:{\"属性\":\"", "", -1)
						attr = strings.Replace(attr, "(产品属性:{", "", -1)
						goodDetail.Attr = strings.Replace(attr, "})", "", -1)
						code := strings.Replace(details[2], "(商家编码:", "", -1)
						goodDetail.Code = strings.Replace(code, ")", "", -1)
						//amount := strings.Replace(details[3], "(产品数量:", "", -1)
						//amount = strings.Replace(amount, ")", "", -1)
						//goodDetail.Amount,err = strconv.Atoi(amount)
						//if err != nil {
						//	fmt.Println("atoi err:", err, " amount:", amount)
						//}
						tmp.DetailInfo = goodDetail
					}
				case 14:
					tmp.Store = cell.Value
				case 15:
					tmp.Platform = cell.Value
				}
			}

			if tmp.OrderNo == "" {
				continue
			}

			//详情中有多个数据
			if len(detailsArr) > 0 {
				for _,v := range detailsArr {
					tmp.DetailInfo = v
					if _, ok := dataMap[key]; ok {
						dataMap[key] = append(dataMap[key], tmp)
					} else {
						dataMap[key] = []ExcelGoodsInfo{tmp}
					}
				}
			} else {
				if _, ok := dataMap[key]; ok {
					dataMap[key] = append(dataMap[key], tmp)
				} else {
					dataMap[key] = []ExcelGoodsInfo{tmp}
				}
			}

		}
	}
	return dataMap
}

func MergeProduct(data map[string][]ExcelGoodsInfo) []ExcelGoodsInfo {
	mergedData := make([]ExcelGoodsInfo, 0)
	for _, v := range data {
		//将每一个订单号-手机 下的多个相同商品合并
		//针对同一个订单号手机号下的订单 使用临时变量聚合
		var tmp = make(map[string]ExcelGoodsInfo)
		for _, goodInfo := range v {
			tmpKey := strings.Replace(goodInfo.DetailInfo.Code + "-" + goodInfo.Store + "-" + goodInfo.Platform, " ", "", -1)
			if exist, ok:= tmp[tmpKey]; ok {
				exist.GoodsAmount += goodInfo.GoodsAmount
				tmp[tmpKey] = exist
			} else {
				tmp[tmpKey] = goodInfo
			}
		}

		for _, vv := range tmp {
			mergedData = append(mergedData, vv)
		}
	}
	return mergedData
}

func SaveData(data []ExcelGoodsInfo) {
	insertSql := "insert into goods_infos (order_no,total_fee,express_fee, goods_amount,receiver,phone,goods_detail,store,platform,date,code,attr) values "
	for _, v := range data {
		insertSql += fmt.Sprintf("(\"%s\",%d, %d, %d,\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"),", v.OrderNo, v.TotalFee, v.ExpressFee, v.GoodsAmount, v.Receiver, v.Phone, strings.Replace(v.DetailInfo.Detail, "\"", "", -1),v.Store,v.Platform, time.Now().Format("2006-01-02"), strings.Replace(v.DetailInfo.Code, "\"","", -1),strings.Replace(v.DetailInfo.Attr, "\"","", -1))
	}

	length := len(insertSql)
	if length > 100 {
		_, err := DbSms.Exec(insertSql[0 : length-1])
		if err != nil {
			log.Println("exec SaveData sql err:", err, " sql:", insertSql)
		}
	}
}

func (self *Apn) SendSms(w web.ResponseWriter, r *web.Request) {
	date := ""
	if t, ok := r.Request.URL.Query()["date"]; ok {
		date = t[0]
	}

	if date == "" {
		self.okPacket.Code = -1
		self.okPacket.Message = "日期不能为空"
		return
	}

	data := make([]GoodsInfo,0)
	if err := GormDbSms.Select("phone").Where("date = ?", date).Group("phone").Find(&data).Error; err != nil && err != gorm.ErrRecordNotFound {
		log.Println("SendMsg get info err:", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "内部错误"
		return
	}

	self.okPacket.Code = 1
	self.okPacket.Data = data
	return

	for _, v := range data {
		if strings.HasPrefix(v.Phone, "0") {
			v.Phone = "66" + v.Phone[1:]
		}
		url := "http://jp.hicall.ai/apn/sendMsg?phone=" + v.Phone
		method := "GET"

		client := &http.Client {
		}
		req, err := http.NewRequest(method, url, nil)

		if err != nil {
			fmt.Println(err)
			return
		}
		res, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer res.Body.Close()

		_, err = ioutil.ReadAll(res.Body)
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Println(res)
	}
}