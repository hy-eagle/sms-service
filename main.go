package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gocraft/web"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"net/http"
	"sms-service/api"
	"time"
)

var err error

func init() {
	//flag.StringVar(&filePath, "xlsx", "goods.xlsx", "xlsx file")
	//
	api.DbSms, err = sql.Open("mysql", "root:hyTTv587@eagletest@tcp(172.16.50.12:3306)/cs_data")
	if err != nil {
		log.Println("init dbSms error:", err)
	}
	api.DbSms.SetConnMaxLifetime(3 * time.Minute)

	api.GormDbSms, err = gorm.Open(mysql.New(mysql.Config{
		Conn: api.DbSms,
	}), &gorm.Config{})
	if err != nil {
		log.Println("init gormDbSms error:", err)
	}
}

func main() {
	rootRouter := web.New(api.RootContext{}).Middleware((*api.RootContext).Acao)

	apnRouter := rootRouter.Subrouter(api.Apn{}, "/apn")
	apnRouter.Middleware((*api.Apn).PrePackMiddleware)
	apnRouter.Get("/getGoodsInfo", (*api.Apn).GetGoodsInfo)
	apnRouter.Get("/saveExcel", (*api.Apn).SaveExcel)
	apnRouter.Get("/sendMsg", (*api.Apn).SendMsg)
	apnRouter.Get("/sendMsgForTest", (*api.Apn).SendMsgForTest)
	apnRouter.Get("/report", (*api.Apn).Report)
	apnRouter.Post("/uploadFile", (*api.Apn).Upload)
	apnRouter.Get("/startSend", (*api.Apn).SendSms)

	apiRouter := rootRouter.Subrouter(api.Api{}, "/api")
	apiRouter.Get("/downloadFile", (*api.Api).DownloadFile)

	shortRouter := rootRouter.Subrouter(api.Short{}, "/")
	shortRouter.Get("/:decode", (*api.Short).ShortUrlJump)
	ch := make(chan error, 1)
	go func() {
		//http1 server
		ch <- http.ListenAndServe(":8787", rootRouter)
	}()

	err = <-ch
	log.Println("http server err:", err)
}


